import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Item} from "../../models/item/item.module";
import {ShoppingListService} from "../../services/shopping-list/shopping-list.service";
import {ToastService} from "../../services/toast/toast.service";

/**
 * Generated class for the EditShoppingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shopping-item',
  templateUrl: 'edit-shopping-item.html',
})
export class EditShoppingItemPage {
  item: Item;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private shopping: ShoppingListService,
    private toast: ToastService,
    private alertCtrl: AlertController) {
  }

  ionViewWillLoad() {
    this.item = this.navParams.get('item');

  }

  saveItem() {
    this.shopping.editItem(this.item).then(() => {
      this.toast.show(`${this.item.name} saved!`);
      this.navCtrl.setRoot('HomePage');
    }, () => {
      console.log('something wrong happened')
    });
  }

  deleteItem() {
    let alert = this.alertCtrl.create({
      title: 'Delete Item',
      message: 'Do you want to remove this item?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.shopping.removeItem(this.item).then(() => {
              this.toast.show(`${this.item.name} removed!`);
              this.navCtrl.setRoot('HomePage');
            }, (err) => {
              console.log('error:', err)
            })
          }
        }
      ]
    });
    alert.present();
  }

}
